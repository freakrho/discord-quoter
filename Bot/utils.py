class Utils:
    @staticmethod
    def strip_to_left(line: str):
        for c in line:
            if c.isspace():
                line = line[1:]
            else:
                return line
        return line

    @staticmethod
    def first_word(line: str):
        word = ""
        line = Utils.strip_to_left(line)
        for c in line:
            if c.isspace():
                return word
            word += c
        return word