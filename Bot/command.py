class Command:
    def __init__(self, line):
        """
        :param line: str
        """
        parts = line.split(" ")
        self.params = []  # type: list
        self.params_raw = ""
        self.command = ""
        self.is_valid = False

        if len(parts) > 0:
            self.command = parts[0].lower()
            self.is_valid = True
            if len(parts) > 1:
                self.params = parts[1:]
                self.params_raw = " ".join(self.params)
